<?php

namespace Adminsite\Eventos\Facades;

use Illuminate\Support\Facades\Facade;

class Eventos extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'eventos';
	}
}