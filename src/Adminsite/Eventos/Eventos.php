<?php

namespace Adminsite\Eventos;

use Carbon\Carbon, DB, Config;

class Eventos
{
	public function __construct()
	{
		$this->dt = Carbon::now();
		$this->dt->hour   = 0;
		$this->dt->minute = 0;
		$this->dt->second = 0;
	}


	public function hola()
	{
		return 'Gracias por usar Adminsite Eventos!';
	}


	public function hoy()
	{
		return Evento::hoy()->get();
	}


	public function mes($mes = 0, $meses = 1)
	{
		if ($mes > 0) {
			$dt->month = $mes;
		}

		$this->dt->day    = 1; //Primer dia del mes

		$init = $this->dt->toDateTimeString();
		$this->dt->addMonths($meses)->subDay();
		$end  = $this->dt->toDateTimeString();

		return Evento::mes($init, $end)->get();
	}


	public function periodo(\DateTime $inicio, \DateTime $fin)
	{
		return Evento::periodo($inicio, $fin)->get();
	}
}