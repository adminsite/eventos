<?php

namespace Adminsite\Eventos;

use DB;
use Illuminate\Validation\Validator;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;

/**
* Evento Modelo
*/
class Evento extends Eloquent
{
	//Tabla
	protected $table = 'adm_eventos';

	/**
	 * Reglas para la validacion
	 * 
	 * @var Array
	 */
	public static $reglas = array(
		'titulo' => 'required|regex:/^[\p{L}\p{N}\s\p{P}]+$/u',
	);

	public static $mensajes = array(
		'titulo.required' => 'El titulo del evento es requerido.',
		'titulo.regex'    => 'El titulo contiene caracteres invalidos.'
	);

	/**
	 * Instancia Validador
	 * 
	 * @var Illuminate\Validation\Validators
	 */
	protected $validator;

	public function __construct(array $attributes = array(), Validator $validator = null)
	{
		parent::__construct($attributes);
		$this->validator = $validator ?: \App::make('validator');
	}

	/**
	 * Validates current attributes against reglas
	 */
	public function validate()
	{
		$v = $this->validator->make($this->attributes, static::$reglas, static::$mensajes);
		if ($v->passes())
		{
			return true;
		}
		$this->setErrors($v->messages());
		return false;
	}

	/**
	 * Set error message bag
	 * 
	 * @var Illuminate\Support\MessageBag
	 */
	protected function setErrors($errors)
	{
		$this->errors = $errors;
	}

	public function scopeHoy($query)
	{
		return $query->where('inicio', DB::raw('CURDATE()'));
	}

	public function scopeMes($query, $init, $end)
	{
		return $query->whereBetween('inicio', array($init, $end));
	}

	public function scopePeriodo($query, \DateTime $init, \DateTime $end)
	{
		return $query->whereBetween('inicio', array($init, $end));
	}
}