<?php

namespace Adminsite\Eventos\Controllers;

use Adminsite\Eventos\Evento;

use Input,
	Request,
	Response,
	DB,
	File;

class EventoController extends \BaseController
{
	private $response = array(
		'error'=>false
	);

	private $filesTypes = array('image/jpg', 'image/jpeg');

	private $path = 'adm/articulos/';

	public function __construct ()
	{
		//Crear directorio de articulos si no existen
		if (File::isDirectory($this->path) == false) {
			File::makeDirectory($this->path, 0777, true, true);
		}

		$gitignore = 'adm/.gitignore';
		if (File::exists($gitignore) == false) {
			File::put($gitignore, '*');
		}
	}

	public function index()
	{
		$eventos = Evento::paginate(15);
		return Response::json($eventos, '200');
	}

	public function store ()
	{
		DB::beginTransaction();

		try
		{
			$evento = new Evento;

			// Crear Nuevo
			$evento->titulo      = strip_tags(Input::get('title'));
			$evento->lugar       = strip_tags(Input::get('lugar'));
			$evento->descripcion = strip_tags(Input::get('descripcion', ''));
			$evento->color       = Input::get('color');
			$evento->inicio      = Input::get('start');
			$evento->fin         = Input::get('end');
			
			//Validar
			/*if ($evento->validate() == false)
			{
				throw new \Exception(json_encode($evento->errors), 1);
			}*/

			$evento->save();

			DB::commit();
			return Response::json($evento, '200');
		}
		catch(\Exception $e)
		{
			DB::rollBack();
			
			$msg = json_decode($e->getMessage());
			$this->response['error']   = true;
			$this->response['mensaje'] = (is_array($msg) or is_object($msg)) ? $msg : $e->getMessage();
			return Response::json($this->response, '400');
		}
	}

	public function update ($id)
	{
		DB::beginTransaction();

		try
		{		
			//Seleccionar Registro
			$evento = Evento::find($id);
			$evento->titulo      = strip_tags(Input::get('title'));
			$evento->lugar       = strip_tags(Input::get('lugar'));
			$evento->descripcion = strip_tags(Input::get('descripcion', ''));
			$evento->color       = Input::get('color');
			$evento->inicio      = Input::get('start');
			$evento->fin         = Input::get('end');

			//Validar
			/*if ($evento->validate() == false)
			{
				throw new \Exception(json_encode($evento->errors), 1);
			}*/

			$evento->save();

			DB::commit();
			return Response::json($evento, '200');
		}
		catch(\Exception $e)
		{
			DB::rollBack();
			
			$msg = json_decode($e->getMessage());
			$this->response['error']   = true;
			$this->response['mensaje'] = (is_array($msg) or is_object($msg)) ? $msg : $e->getMessage();
			return Response::json($this->response, '400');
		}
	}

	public function destroy ($id)
	{
		$delete = Evento::destroy($id);

		if (!$delete)
		{
			$this->response['error'] = true;
			return Response::json($this->response, '400');
		}

		return Response::json($this->response, '200');
	}
}
