<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEventos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_eventos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo', 80);
			$table->string('lugar', 60);
			$table->string('color', 8);
			$table->datetime('inicio', 30);
			$table->datetime('fin', 30);
			$table->longText('descripcion');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eventos');
	}

}
