<?php

/**
 * Filtro para la comunicacion de la API
 *
 * @return Bool
 */
App::after(function($request, $response)
{
	if (Config::has('adm.cors')) {
		$response->header('Access-Control-Allow-Origin', Config::get('adm.cors'));
	}

	$response->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
	$response->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, X_FILENAME, Content-Type, Content-Range, Content-Disposition, Content-Description');
});

Route::group(array(
	'prefix'    => 'adm/api/v1', 
	'before'    => 'api', 
	'namespace' => '\Adminsite\Eventos\Controllers'
), function()
{
	Route::resource('eventos', 'EventoController');
});